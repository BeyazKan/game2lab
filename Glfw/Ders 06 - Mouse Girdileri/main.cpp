﻿// Game2Lab - Mouse Girdileri
//
// 02.04.2014 tarihinde Mustafa Oğuz tarafından oluşturulmuştur.
// Kopyalama hakları Mustafa Oğuz'a aittir ve tüm hakları saklıdır.
 
#include <GLFW/glfw3.h>
#include <iostream>
 
using namespace std;
 
// Global Değişkenler
// 2D düzlemde koordinat değişkenleri
GLfloat x = 0.0f;
GLfloat y = 0.0f;
 
// Klavye mesajlarının işlendiği callback fonksiyonu
static void klavye_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    // Esc tuşuna basıldığında, pencereyi kapat
    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
    // Klavye Tuşlarına göre çıktı ve 2D Karemizi hareket ettirme
    else if(key == GLFW_KEY_W && action == GLFW_PRESS){
        cout << "Klavye Girdisi -> W tusuna basildi" << endl;
        y += 0.1f;
    }
    else if(key == GLFW_KEY_S && action == GLFW_PRESS){
        cout << "Klavye Girdisi -> S tusuna basildi" << endl;
        y -= 0.1f;
    }
    else if(key == GLFW_KEY_A && action == GLFW_PRESS){
        cout << "Klavye Girdisi -> A tusuna basildi" << endl;
        x -= 0.1f;
    }
 
    else if(key == GLFW_KEY_D && action == GLFW_PRESS){
        cout << "Klavye Girdisi -> D tusuna basildi" << endl;
        x += 0.1f;
    }
}
 
static void mouseBut_callback(GLFWwindow *window, int button, int action, int mods)
{
    if(button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
    {
        cout << "Mouse Girdisi -> Sol tusa basildi" << endl;
        x -= 0.1f;
    }
    else if(button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
    {
        cout << "Mouse Girdisi -> Sag tusa basildi" << endl;
        x += 0.1f;
    }
    else if(button == GLFW_MOUSE_BUTTON_MIDDLE && action == GLFW_PRESS)
    {
        cout << "Mouse Girdisi -> Orta tusa basildi" << endl;
        x = 0.0f;
        y = 0.0f;
    }
}
 
int main(void)
{
    /*  GLFWwindow yapı (struct) türünden,
        window pointer nesnesi tanımlanıyor. */
    GLFWwindow* window;
    /* Glfw Kütüphane Başlatılıyor. */
    // glfwInit = Kütüphane başlatma fonksiyonu...
 
    if (glfwInit())
        cout << "GLFW -> OK : Kutuphane basariyla baslatildi." << endl;
    else{
        cout << "GLFW -> HATA : Kutuphane baslatilamadi." << endl;
        return -1;
    }
 
    /* Pencere modunda, pencere oluşturuluyor */
    // glfwCreateWindow = Pencere yaratma fonksiyonu.
    window = glfwCreateWindow(640, 480, "Game2Lab : Klavye Girdisi", NULL, NULL);
    if (window)
        cout << "GLFW -> OK : Pencere olusturuldu." << endl;
    else{
        cout << "GLFW -> HATA : Pencere olusturulamadi." << endl;
        glfwTerminate();
        return -1;
    }
 
    // glfwMakeContextCurrent = Pencere'ye varsayılan içerik tanımlanıyor.
    glfwMakeContextCurrent(window);
 
    // Klavye Girdisi gerçekleştiğinde çağırılacak fonksiyonu belirten fonksiyon
    glfwSetKeyCallback(window, klavye_callback);
    // Mouse Girdisi gerçekleştiğinde çağırılacak fonksiyonu belirten fonksiyon
    glfwSetMouseButtonCallback(window, mouseBut_callback);
 
    /* Programın sonuça varıp, kapanmaması için döngüye sokuluyor */
    // glfwWindowShouldClose = Pencerenin kapanıp kapanmadığını tespit eden fonksiyon
    while (!glfwWindowShouldClose(window))
    {
        /* Render Alani */
        float ratio;
        int width, height;
 
        // Pencere boyutu alınıyor. (Genişlik ve Yükseklik)
        glfwGetFramebufferSize(window, &width, &height);
        ratio = width / (float) height;
 
        // OpenGL Komutları -- Örnek olması için koyuldu.
        // OpenGL Derslerinde İşlenecek
        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(-ratio, ratio, -1.f, 1.f, 1.f, -1.f);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
 
        glTranslatef(x, y, 0.0f);
        glBegin(GL_TRIANGLES);
        glColor3f(1.0f, 0.0f, 0.0f);
        glVertex2f(-0.3f, -0.3);
        glVertex2f(0.3f, -0.3f);
        glVertex2f(0.0f, 0.3f);
        glEnd();
 
        /*  glfwSwapBuffers = Pencere içeriğindeki
            ön ve arka bufferı değiştiren fonksiyon */
        glfwSwapBuffers(window);
 
        /*  Pencere ve işlem mesajlarını,
            mesaj kuyruğuna alan fonksiyon */
        glfwPollEvents();
    }
 
    // Pencere nesnesi sonlandırılıyor.
    glfwDestroyWindow(window);
    // Glfw Kütüphane Sonlandırılıyor.
    glfwTerminate();
    return 0;
}