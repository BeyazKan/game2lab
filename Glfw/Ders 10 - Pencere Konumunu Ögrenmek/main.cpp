﻿// Game2Lab - Pencere Konumunu Öğrenmek
//
// 07.04.2014 tarihinde Mustafa Oğuz tarafından oluşturulmuştur.
// Kopyalama hakları Mustafa Oğuz'a aittir ve tüm hakları saklıdır.

// Konsol temizleme sabitleri
#ifdef _WIN32
#define CLEAR "cls"
#else // Windows dışı işletim sistemleri (Other OS) için...
#define CLEAR "clear"
#endif

#include <GLFW/glfw3.h>
#include <iostream>
#include <cstdlib>

using namespace std;

// Varsayılan Pencere Başlığı
#define TITLE   "Game2Lab : Pencere Konumunu Öğrenmek"

// Varsayılan Sabit Pencere Boyutları
#define WIDTH       640
#define HEIGHT      480

// Pencere boyut değişkenleri
int width;
int height;

// Pencere konum (pos) değişkenleri
int xpos;
int ypos;

// Prototip
static void klavye_callback(GLFWwindow *window, int key, int scancode, int action, int mods);
void pencerePos_callback(GLFWwindow *window, int xpos, int ypos);

// Mouse mesajlarının işlendiği callback fonksiyonu
static void mouseBut_callback(GLFWwindow *window, int button, int action, int mods)
{
}

int main(void)
{

    /*  GLFWwindow yapı (struct) türünden,
        window pointer nesnesi tanımlanıyor. */
    GLFWwindow* window;

    /* Glfw Kütüphane Başlatılıyor. */
    // glfwInit = Kütüphane başlatma fonksiyonu...

    if (glfwInit())
        cout << "GLFW -> OK : Kutuphane basariyla baslatildi." << endl;
    else{
        cout << "GLFW -> HATA : Kutuphane baslatilamadi." << endl;
        return -1;
    }

    /* Pencere modunda, pencere oluşturuluyor */
    // glfwCreateWindow = Pencere yaratma fonksiyonu.
    window = glfwCreateWindow(WIDTH, HEIGHT, TITLE, NULL, NULL);
    if (window)
        cout << "GLFW -> OK : Pencere olusturuldu." << endl;
    else{
        cout << "GLFW -> HATA : Pencere olusturulamadi." << endl;
        glfwTerminate();
        return -1;
    }

    // glfwMakeContextCurrent = Pencere'ye varsayılan içerik tanımlanıyor.
    glfwMakeContextCurrent(window);

    // Klavye Girdisi gerçekleştiğinde çağırılacak fonksiyon (callback)
    glfwSetKeyCallback(window, klavye_callback);
    // Mouse Girdisi gerçekleştiğinde çağırılacak fonksiyon (callback)
    glfwSetMouseButtonCallback(window, mouseBut_callback);
    // Pencere yeniden konumlandırıldığında çağırılacak fonksiyon (callback)
    glfwSetWindowPosCallback(window, pencerePos_callback);

    /* Programın sonuça varıp, kapanmaması için döngüye sokuluyor */
    // glfwWindowShouldClose = Pencerenin kapanıp kapanmadığını tespit eden fonksiyon
    while (!glfwWindowShouldClose(window))
    {
        /* Render Alani */
        float ratio;
        int width, height;

        // OpenGL içerik boyutu alınıyor. (Genişlik ve Yükseklik)
        glfwGetFramebufferSize(window, &width, &height);
        ratio = width / (float) height;

        // OpenGL Komutları -- Örnek olması için koyuldu.
        // OpenGL Derslerinde İşlenecek
        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(-ratio, ratio, -1.f, 1.f, 1.f, -1.f);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        glBegin(GL_TRIANGLE_FAN);
        glColor3f(0.0f, 1.0f, 0.0f);
        glVertex2f(-0.4, 0.25);
        glVertex2f(-0.3f, -0.2);
        glVertex2f(0.3f, -0.2);
        glVertex2f(0.4, 0.25);
        glVertex2f(0.0, 0.6);
        glEnd();

        /*  glfwSwapBuffers = Pencere içeriğindeki
            ön ve arka bufferı değiştiren fonksiyon */
        glfwSwapBuffers(window);

        /*  Pencere ve işlem mesajlarını,
            mesaj kuyruğuna alan fonksiyon */
        glfwPollEvents();
    }

    // Pencere nesnesi sonlandırılıyor.
    glfwDestroyWindow(window);
    // Glfw Kütüphane Sonlandırılıyor.    glfwTerminate();
    return 0;
}

// Klavye mesajlarının işlendiği callback fonksiyonu
static void klavye_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    // Esc tuşuna basıldığında, pencereyi kapat
    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
    else if(key == GLFW_KEY_F1 && action == GLFW_PRESS){

        glfwGetWindowPos(window, &xpos, &ypos);
        cout << "glfwGetWindowPos() fonksiyonu" << endl;
        cout << "------------------------------------------" << endl;
        cout << "GLFW -> Pencere konum bilgisi." << endl;
        cout << "------------------------------------------" << endl;
        cout << "x koordinati :" << xpos << endl;
        cout << "y koordinati :" << ypos << endl;
        cout << "------------------------------------------" << endl << endl;
    }
}

void pencerePos_callback(GLFWwindow *window, int xpos, int ypos)
{
    // Konsol penceresi temizleme.
    system(CLEAR);

    cout << "glfwSetWindowPosCallback() fonksiyonu" << endl;
    cout << "------------------------------------------" << endl;
    cout << "GLFW -> Pencere konum bilgisi." << endl;
    cout << "------------------------------------------" << endl;
    cout << "x koordinati :" << xpos << endl;
    cout << "y koordinati :" << ypos << endl;
    cout << "------------------------------------------" << endl << endl;
}
