﻿// Game2Lab - Pencere Oluşturma İpuçları
//
// 20.06.2014 tarihinde Mustafa Oğuz tarafından oluşturulmuştur.
// Kopyalama hakları Mustafa Oğuz'a aittir ve tüm hakları saklıdır.

// Konsol temizleme sabitleri
#ifdef _WIN32
#define CLEAR "cls"
#else // Windows dışı işletim sistemleri (Other OS) için...
#define CLEAR "clear"
#endif

#include <GLFW/glfw3.h>
#include <iostream>

using namespace std;

// Varsayılan Pencere Başlığı
#define TITLE   "Game2Lab : Pencere İpuçları"

// Varsayılan Sabit Pencere Boyutları
#define WIDTH       640
#define HEIGHT      480

/*     Prototipler     */
// Hata Çıktı
static void error_callback(int error, const char* description);
// Klavye Callback
static void klavye_callback(GLFWwindow *window, int key, int scancode, int action, int mods);
// Fare Callback
static void mouseBut_callback(GLFWwindow *window, int button, int action, int mods);

int main(void)
{

    /*  GLFWwindow yapı (struct) türünden,
        window pointer nesnesi tanımlanıyor. */
    GLFWwindow* window;

    // Hata gerçekleştiğinde çağırılacak fonksiyon (callback)
    glfwSetErrorCallback(error_callback);

    /* Glfw Kütüphane Başlatılıyor. */
    // glfwInit = Kütüphane başlatma fonksiyonu...
    if (glfwInit())
        cout << "GLFW -> OK : Kutuphane basariyla baslatildi." << endl;
    else{
        cout << "GLFW -> HATA : Kutuphane baslatilamadi." << endl;
        return -1;
    }

    // Glfw pencere oluşturma ipuçları
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    /* Pencere modunda, pencere oluşturuluyor */
    // glfwCreateWindow = Pencere yaratma fonksiyonu.
    window = glfwCreateWindow(WIDTH, HEIGHT, TITLE, NULL, NULL);
    if (window)
        cout << "GLFW -> OK : Pencere olusturuldu." << endl;
    else{
        cout << "GLFW -> HATA : Pencere olusturulamadi." << endl;
        glfwTerminate();
        return -1;
    }

    // glfwMakeContextCurrent = Pencere'ye varsayılan içerik tanımlanıyor.
    glfwMakeContextCurrent(window);

    // Klavye Girdisi gerçekleştiğinde çağırılacak fonksiyon (callback)
    glfwSetKeyCallback(window, klavye_callback);
    // Mouse Girdisi gerçekleştiğinde çağırılacak fonksiyon (callback)
    glfwSetMouseButtonCallback(window, mouseBut_callback);

    /* Programın sonuça varıp, kapanmaması için döngüye sokuluyor */
    // glfwWindowShouldClose = Pencerenin kapanıp kapanmadığını tespit eden fonksiyon
    while (!glfwWindowShouldClose(window))
    {
        /* Render Alani */
        float ratio;
        int width, height;

        // OpenGL içerik boyutu alınıyor. (Genişlik ve Yükseklik)
        glfwGetFramebufferSize(window, &width, &height);
        ratio = width / (float) height;

        // OpenGL Komutları -- Örnek olması için koyuldu.
        // OpenGL Derslerinde İşlenecek
        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(-ratio, ratio, -1.f, 1.f, 1.f, -1.f);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        glBegin(GL_TRIANGLE_FAN);
        glColor3f(1.0f, 0.0f, 0.0f);
        glVertex2f(-0.4, 0.2);
        glVertex2f(-0.4, -0.2);
        glVertex2f(-0.3f, -0.35);
        glVertex2f(0.3f, -0.35);
        glVertex2f(0.4, -0.2);
        glVertex2f(0.4, 0.2);
        glVertex2f(0.25, 0.4);
        glVertex2f(-0.25, 0.4);
        glEnd();

        /*  glfwSwapBuffers = Pencere içeriğindeki
            ön ve arka bufferı değiştiren fonksiyon */
        glfwSwapBuffers(window);

        /*  Pencere ve işlem mesajlarını,
            mesaj kuyruğuna alan fonksiyon */
        glfwPollEvents();
    }

    // Pencere nesnesi sonlandırılıyor.
    glfwDestroyWindow(window);
    // Glfw Kütüphane Sonlandırılıyor.    glfwTerminate();
    return 0;
}

// Olası hataların çıktılandığı callback fonksiyonu
static void error_callback(int error, const char* description)
{
    // Genel Açıklama
    cout << "GLFW -> HATA : " << description << endl;

    // Detaylı Açıklama
    if(error == GLFW_NOT_INITIALIZED){
        cout << "GLFW -> ERROR CODE : GLFW_NOT_INITIALIZED" << endl;
        cout << "GLFW -> HATA SEBEBI : Glfw kutuphanesi baslatilmadi..." << endl << endl;
    }
    else if(error == GLFW_NO_CURRENT_CONTEXT){
        cout << "GLFW -> ERROR CODE : GLFW_NO_CURRENT_CONTEXT" << endl;
        cout << "GLFW -> HATA SEBEBI : Varsayilan OpenGL icerigi bulunamadi..." << endl << endl;
    }
    else if(error == GLFW_INVALID_ENUM){
        cout << "GLFW -> ERROR CODE : GLFW_INVALID_ENUM" << endl;
        cout << "GLFW -> HATA SEBEBI : Fonksiyon parametrelerinden bir numerator deger eksik girilmis..." << endl << endl;
    }
    else if(error == GLFW_INVALID_VALUE){
        cout << "GLFW -> ERROR CODE : GLFW_INVALID_VALUE" << endl;
        cout << "GLFW -> HATA SEBEBI : Fonksiyon parametrelerinden bir deger eksik girilmis..." << endl << endl;
    }
     else if(error == GLFW_OUT_OF_MEMORY){
        cout << "GLFW -> ERROR CODE : GLFW_OUT_OF_MEMORY" << endl;
        cout << "GLFW -> HATA SEBEBI : Bellekte yeterli alan yok..." << endl << endl;
    }
     else if(error == GLFW_API_UNAVAILABLE){
        cout << "GLFW -> ERROR CODE : GLFW_API_UNAVAILABLE" << endl;
        cout << "GLFW -> HATA SEBEBI : Bilinmeyen bir api..." << endl << endl;
    }
     else if(error == GLFW_VERSION_UNAVAILABLE){
        cout << "GLFW -> ERROR CODE : GLFW_VERSION_UNAVAILABLE " << endl;
        cout << "GLFW -> HATA SEBEBI : Sistem versiyonu bilinmiyor..." << endl  << endl;
    }
    else if(error == GLFW_PLATFORM_ERROR){
        cout << "GLFW -> ERROR CODE : GLFW_PLATFORM_ERROR" << endl;
        cout << "GLFW -> HATA SEBEBI : Bilinmeyen isletim sistemi..." << endl  << endl;
    }
    else if(error == GLFW_FORMAT_UNAVAILABLE){
        cout << "GLFW -> ERROR CODE : GLFW_FORMAT_UNAVAILABLE" << endl;
        cout << "GLFW -> HATA SEBEBI : Bilinmeyen format..." << endl  << endl;
    }
}

// Klavye mesajlarının işlendiği callback fonksiyonu
static void klavye_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    // Esc tuşuna basıldığında, pencereyi kapat
    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
}

// Mouse mesajlarının işlendiği callback fonksiyonu
static void mouseBut_callback(GLFWwindow *window, int button, int action, int mods)
{
}