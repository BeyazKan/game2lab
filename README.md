﻿#MSOguz - 3D Grafik ve Oyun Programlama Dersleri
![Alt text](http://3.bp.blogspot.com/-2QEpSxGPfgs/UzF16iEJndI/AAAAAAAAAJ4/RTHpc8WeZwY/s1600/game2lab-logo.png)

Blog sayfamızda yer alan derslerin kaynak kodları zamanla burada yayınlanacak.

##Glfw Dersleri
----

* Glfw Nedir? - [Ders Sayfası](https://www.msoguz.com/2014/02/glfw-nedir.html)
* GLFW3 Kütüphanesini Derlemek (Compile) - [Ders Sayfası](https://www.msoguz.com/2014/02/glfw3-kutuphanesini-derleme-compile.html)
* GLFW Projesi Oluşturma ve Yapılandırma - [Ders Sayfası](https://www.msoguz.com/2014/03/glfw-projesi-olusturma-ve-yapilandirma.html)
* Glfw ile Pencere Oluşturmak - [Ders Sayfası](https://www.msoguz.com/2014/03/glfw-ile-pencere-olusturmak.html)
* Glfw ile Klavye Girdileri - Bölüm 1 - [Ders Sayfası](https://www.msoguz.com/2014/04/glfw-ile-klavye-girdileri-bolum-1.html)
* Glfw ile Fare Girdileri - [Ders Sayfası](https://www.msoguz.com/2014/04/glfw-ile-fare-girdileri.html)
* Glfw ile Pencere Başlığı Değiştirme - [Ders Sayfası](https://www.msoguz.com/2014/04/glfw-ile-pencere-basligini-degistirme.html)
* Glfw Pencere Boyutlarını Öğrenmek - [Ders Sayfası](https://www.msoguz.com/2014/05/glfw-pencere-boyutlarini-ogrenmek.html)
* Glfw Penceresini Yeniden Boyunlandırma - [Ders Sayfası](https://www.msoguz.com/2014/05/glfw-penceresini-yeniden-boyutlandirma.html)
* Glfw Pencere Konumunu Öğrenmek - [Ders Sayfası](https://www.msoguz.com/2014/05/glfw-pencere-konumunu-ogrenmek.html)
* Glfw Penceresini Yeniden Konumlandırma - [Ders Sayfası](https://www.msoguz.com/2014/05/glfw-penceresini-yeniden-konumlandirma.html)
* Glfw ile Hata Kontrol - [Ders Sayfası](https://www.msoguz.com/2014/05/glfw-ile-hata-kontrol.html)
* Glfw Pencere Oluşturma İpuçları - [Ders Sayfası](https://www.msoguz.com/2014/06/glfw-pencere-olusturma-ipuclari.html)
* Glfw ile Klavye Girdileri - Bölüm 2 - [Ders Sayfası](https://www.msoguz.com/2014/06/glfw-klavye-girdileri-bolum-2.html)
* Glfw ile Fare Tekerleği Girdisi - [Ders Sayfası](https://www.msoguz.com/2014/07/glfw-ile-fare-tekerlegi-girdisi.html)

----

##OpenGL Dersleri
----

* OpenGL nedir? - [Ders Sayfası](https://www.msoguz.com/2014/05/opengl-nedir.html)
* OpenGL Çalışma Mantığı ve Tasarımı - [Ders Sayfası](https://www.msoguz.com/2014/06/opengl-calisma-mantigi-ve-tasarimi.html)
* OpenGL Veri Tipleri - [Ders Sayfası](https://www.msoguz.com/2014/06/opengl-veri-tipleri.html)

----
Dersler hazırlandıkça, güncellenecek.

[MSOguz.com - Mustafa Oğuz](https://www.msoguz.com/)
